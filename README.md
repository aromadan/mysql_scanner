# MySQL Scanner

A scanner to detect if MySQL running on a port on a host, and output details about the server. 

## Installation

```
git clone https://aromadan@bitbucket.org/aromadan/mysql_scanner.git
cd mysql_scanner
```

## Usage

See flags (user can specify host, port and timeout)
```
go run . -h
``` 
Scan a specific host and port
```
go run . -i=54.78.108.130 -p=3306 -t=2
```
Scan default host and port (localhost:3306) with timeout 10s
```
go run .
```

## Automated tests
```
go test -v
```

## TO DO

What I would work on next to make this a little cleaner and more robust:

1. Add more test coverage.
2. Mock out net connection calls in tests.
3. Parse MySQL initial handshake packet versions other than 10.
4. Differentiate between connection and read timeouts.
5. Split code into multiple packages.
 
