package main

import (
	"fmt"
	"reflect"
	"testing"

	"gotest.tools/assert"
)

func TestDecodeErrorPacket(t *testing.T) {
	//64.140.146.90:3306
	payload := []byte{255, 106, 4, 72, 111, 115, 116, 32, 39, 50,
		48, 49, 46, 50, 49, 51, 46, 49, 56, 48, 46, 50, 53, 39, 32,
		105, 115, 32, 110, 111, 116, 32, 97, 108, 108, 111, 119, 101,
		100, 32, 116, 111, 32, 99, 111, 110, 110, 101, 99, 116, 32,
		116, 111, 32, 116, 104, 105, 115, 32, 77, 121, 83, 81, 76, 32,
		115, 101, 114, 118, 101, 114}
	testPacket := &InitialHandshakePacket{}
	err := testPacket.Decode(payload)
	exptErr := "MySQL Error (1130): Host '201.213.180.25' is not allowed to connect to this MySQL server"
	assert.Equal(t, err.Error(), exptErr)
}

func TestDecodeV9(t *testing.T) {
	payload := []byte{9, 0, 0, 0}
	testPacket := &InitialHandshakePacket{}
	err := testPacket.Decode(payload)
	exptErr := "Not Supported MySQL version"
	assert.Equal(t, err.Error(), exptErr)
}

func TestDecodeBadFiller(t *testing.T) {
	payload := []byte{10, 53, 46, 55, 46, 49, 50, 0, 243, 31, 0, 0, 13,
		50, 72, 127, 113, 111, 63, 86, 1, 255, 255, 8, 2, 0, 255, 193, 21, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 55, 35, 66, 26, 103, 60, 6, 35, 46, 115, 37,
		98, 0, 109, 121, 115, 113, 108, 95, 110, 97, 116, 105, 118, 101, 95,
		112, 97, 115, 115, 119, 111, 114, 100, 0}
	testPacket := &InitialHandshakePacket{}
	err := testPacket.Decode(payload)
	exptErr := "Failed to decode filler byte (not a valid V10 packet)"
	assert.Equal(t, err.Error(), exptErr)
}

func TestDecode(t *testing.T) {
	payload := []byte{10, 53, 46, 55, 46, 49, 50, 0, 243, 31, 0, 0, 13,
		50, 72, 127, 113, 111, 63, 86, 0, 255, 255, 8, 2, 0, 255, 193, 21, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 55, 35, 66, 26, 103, 60, 6, 35, 46, 115, 37,
		98, 0, 109, 121, 115, 113, 108, 95, 110, 97, 116, 105, 118, 101, 95,
		112, 97, 115, 115, 119, 111, 114, 100, 0}

	testPacket := &InitialHandshakePacket{}
	err := testPacket.Decode(payload)
	assert.Equal(t, err, nil)
	assert.Equal(t, testPacket.ProtocolVersion, uint8(10))
	assert.Equal(t, testPacket.ServerVersion, "5.7.12")
	assert.Equal(t, testPacket.ConnectionID, uint32(8179))
	assert.Equal(t, testPacket.CharacterSet, "latin1_swedish_ci")

	for _, flag := range Status {
		if flag == "ServerStatusAutocommit" {
			assert.Equal(t, testPacket.Status[flag], true)
		} else {
			assert.Equal(t, testPacket.Status[flag], false)
		}
	}

	for _, flag := range Capabilities {
		assert.Equal(t, testPacket.Capabilities[flag], true)
	}
	assert.Equal(t, testPacket.AuthPluginDataLen, uint8(21))

	exptAPDBytes := []byte{13, 50, 72, 127, 113, 111, 63, 86, 55, 35, 66, 26, 103, 60, 6, 35, 46, 115, 37, 98, 0}
	assert.Equal(t, testPacket.AuthPluginData, string(exptAPDBytes))
	assert.Equal(t, testPacket.AuthPluginName, "mysql_native_password")
}

func TestTrimOKPacket(t *testing.T) {
	data := []byte{74, 0, 0, 0, 10, 53, 46, 55, 46, 49, 50, 0, 243, 31, 0, 0, 13,
		50, 72, 127, 113, 111, 63, 86, 0, 255, 255, 8, 2, 0, 255, 193, 21, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 55, 35, 66, 26, 103, 60, 6, 35, 46, 115, 37,
		98, 0, 109, 121, 115, 113, 108, 95, 110, 97, 116, 105, 118, 101, 95,
		112, 97, 115, 115, 119, 111, 114, 100, 0, 0, 0, 0, 0}

	testPayload, err := TrimPacket(data, 78)
	assert.Equal(t, err, nil)

	exptPayload := data[4:78]
	if reflect.DeepEqual(testPayload, exptPayload) == false {
		msg := fmt.Sprintf("expected: %b\ngot: %b", exptPayload, testPayload)
		t.Errorf(msg)
	}
}

func TestTrimErrorPacket(t *testing.T) {
	data := []byte{74, 0, 0, 0, 10, 53, 46, 55, 46, 49, 50, 0, 243, 31, 0, 0, 13,
		50, 72, 127, 113, 111, 63, 86, 0, 255, 255, 8, 2, 0, 255, 193, 21, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 55, 35, 66, 26, 103, 60, 6, 35, 46, 115, 37,
		98, 0, 109, 121, 115, 113, 108, 95, 110, 97, 116, 105, 118, 101, 95,
		112, 97, 115, 115, 119, 111, 114, 100, 0, 0, 0, 0, 0}

	_, err := TrimPacket(data, 74)
	assert.Equal(t, err.Error(), "Not a MySQL server")
}

func TestSetCapabilities(t *testing.T) {
	part1 := []byte{8, 4}
	part2 := []byte{2, 1}

	testPacket := &InitialHandshakePacket{}
	testPacket.SetCapabilities(part2, part1)

	// part1 + part2 = [1 00000010 00000100 00001000]
	// so that's 4th, 11th, 18th and 25th bits set to 1
	for i, cap := range Capabilities {
		if i == 3 {
			assert.Equal(t, testPacket.Capabilities[cap], true)
		} else if i == 10 {
			assert.Equal(t, testPacket.Capabilities[cap], true)
		} else if i == 17 {
			assert.Equal(t, testPacket.Capabilities[cap], true)
		} else if i == 24 {
			assert.Equal(t, testPacket.Capabilities[cap], true)
		} else {
			assert.Equal(t, testPacket.Capabilities[cap], false)
		}
	}
}

func TestGetPacketConnectionError(t *testing.T) {
	_, _, err := GetPacket("not.a.real.host", "3306", 2)
	assert.Equal(t, err.Error(), "dial tcp: lookup not.a.real.host: no such host")
}

func TestGetPacketConnectionTimeout(t *testing.T) {
	_, _, err := GetPacket("8.8.8.8", "3306", 2)
	assert.Equal(t, err.Error(), "dial tcp 8.8.8.8:3306: i/o timeout")
}

func TestGetPacketReadError(t *testing.T) {
	_, _, err := GetPacket("8.8.8.8", "53", 4)
	assert.Equal(t, err.Error(), "EOF")
}

func TestGetPacketReadTimeout(t *testing.T) {
	_, _, err := GetPacket("google.com", "80", 2)
	// not checking the exact error message as it can vary
	if err == nil {
		t.Errorf("Expected conn.Read to timeout")
	}
}
