package main

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"time"
)

// InitialHandshakePacket holds values that we decode from payload
// that we will want to show to the user at the end of the scan.
// https://dev.mysql.com/doc/internals/en/connection-phase-packets.html
type InitialHandshakePacket struct {
	ProtocolVersion   uint8
	ServerVersion     string
	ConnectionID      uint32
	CharacterSet      string
	Status            map[string]bool
	Capabilities      map[string]bool
	AuthPluginDataLen uint8
	AuthPluginData    string
	AuthPluginName    string
}

// SetCapabilities will decode the input bytes and set corresponding flags in InitialHandshakePacket.Capabilities
func (mySQLInfo *InitialHandshakePacket) SetCapabilities(capBytes2, capBytes1 []byte) {
	capLower := binary.LittleEndian.Uint16(capBytes1)
	capUpper := binary.LittleEndian.Uint16(capBytes2)
	capFlags := uint32(capUpper)<<16 | uint32(capLower)
	mySQLInfo.Capabilities = make(map[string]bool)
	for i, cap := range Capabilities {
		mySQLInfo.Capabilities[cap] = capFlags&(1<<i) != 0
	}
}

// SetStatus will decode status flag bytes and set corresponding InitialHandshakePacket.Status
func (mySQLInfo *InitialHandshakePacket) SetStatus(sfBytes []byte) {
	statFlags := binary.LittleEndian.Uint16(sfBytes)
	mySQLInfo.Status = make(map[string]bool)
	for i, flag := range Status {
		mySQLInfo.Status[flag] = statFlags&(1<<i) != 0
	}
}

// Print will pretty print the mySQLInfo.
func (mySQLInfo *InitialHandshakePacket) Print() {
	// Converting String to JSON
	strJSON, err := json.MarshalIndent(mySQLInfo, "", "    ")
	if err == nil {
		fmt.Println(string(strJSON))
	} else {
		fmt.Println("Unable to print MySQL info.")
	}
}

// Max returns the max of two input integers
func Max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

// DecodeErrorPacket decodes the MySQL Error Packet,
func DecodeErrorPacket(payload []byte) string {
	pos := 1
	errorCodeBytes := payload[pos : pos+2]
	errorCode := binary.LittleEndian.Uint16(errorCodeBytes)
	errorMsgBytes := payload[pos+2:]
	errorMsg := string(errorMsgBytes)
	return fmt.Sprintf("MySQL Error (%d): %s", errorCode, errorMsg)
}

// Decode the payload and populate mySQLInfo
func (mySQLInfo *InitialHandshakePacket) Decode(payload []byte) error {
	pos := 0

	if payload[pos] == 255 {
		errMsg := DecodeErrorPacket(payload)
		return errors.New(errMsg)
	}

	// protocol version is the first byte
	mySQLInfo.ProtocolVersion = payload[pos]
	if mySQLInfo.ProtocolVersion != 10 {
		return errors.New("Not Supported MySQL version")
	}
	pos = pos + 1

	// find terminal character to extract server version
	idx := bytes.IndexByte(payload, byte(0x00))
	mySQLInfo.ServerVersion = string(payload[pos:idx])
	pos = idx + 1

	// next 4 bytes are connection id
	cidBytes := payload[pos : pos+4]
	mySQLInfo.ConnectionID = binary.LittleEndian.Uint32(cidBytes)
	pos = pos + 4

	// string[8] auth-plugin-data-part-1
	// will need to combine with auth-plugin-data-part-2
	authPluginPart1 := payload[pos : pos+8]
	mySQLInfo.AuthPluginData = string(authPluginPart1)
	pos = pos + 8

	// MySQL V10 should have a filler byte that is equal 0
	filler := payload[pos]
	if filler != 0 {
		return errors.New("Failed to decode filler byte (not a valid V10 packet)")
	}
	pos = pos + 1

	// capability flags (lower 2 bytes)
	capBytes1 := payload[pos : pos+2]
	pos = pos + 2

	if len(payload) < pos {
		// not an error, but there is no more data in the packet
		return nil
	}

	// 1 byte character set
	mySQLInfo.CharacterSet = CharSet[uint8(payload[pos])]
	pos = pos + 1

	// 2 bytes status flag
	sfBytes := payload[pos : pos+2]
	mySQLInfo.SetStatus(sfBytes)
	pos = pos + 2

	// capability flags (upper 2 bytes), need to combine with lower 2 byes.
	capBytes2 := payload[pos : pos+2]
	mySQLInfo.SetCapabilities(capBytes2, capBytes1)
	pos = pos + 2

	// 1 byte auth-plugin-data
	if mySQLInfo.Capabilities["clientPluginAuth"] {
		mySQLInfo.AuthPluginDataLen = payload[pos]
	}
	// update position by 1 and skip 10 reserved bytes
	pos = pos + 11

	// auth-plugin-data-part-2
	if mySQLInfo.Capabilities["clientSecureConnection"] {
		len := Max(13, int(mySQLInfo.AuthPluginDataLen)-8)
		authPluginPart2 := payload[pos : pos+len]
		mySQLInfo.AuthPluginData = mySQLInfo.AuthPluginData + string(authPluginPart2)
		pos = pos + len
	}

	// string[NUL] auth-plugin name
	if mySQLInfo.Capabilities["clientPluginAuth"] {
		idx := bytes.IndexByte(payload[pos:], byte(0x00))
		if idx == -1 {
			mySQLInfo.AuthPluginName = string(payload[pos:])
		} else {
			mySQLInfo.AuthPluginName = string(payload[pos : pos+idx])
		}
	}

	return nil
}

// TrimPacket data by removing header and extra bytes that were appended during conn.Read
// The result is the payload (MySQL Initial Handshake Packet).
func TrimPacket(data []byte, numBytes int) ([]byte, error) {
	// first 4 bytes are the header (3 bytes payload length, 1 byte sequence number)
	plBytes := []byte{data[0], data[1], data[2], 0}
	payloadLength := binary.LittleEndian.Uint32(plBytes)
	if uint32(numBytes) != payloadLength+4 {
		return nil, errors.New("Not a MySQL server")
	}
	return data[4 : payloadLength+4], nil
}

// GetPacket attempts to create a tcp connection to host:port and read data from the connection.
func GetPacket(host string, port string, timeout int) ([]byte, int, error) {
	conn, connectionErr := net.DialTimeout("tcp", host+":"+port, time.Second*time.Duration(timeout))
	if connectionErr != nil {
		return nil, 0, connectionErr
	}
	defer conn.Close()

	conn.SetReadDeadline(time.Now().Add(time.Second * time.Duration(timeout)))
	data := make([]byte, 1024)
	numBytes, readErr := conn.Read(data)

	if readErr != nil {
		return nil, 0, readErr
	}

	return data, numBytes, nil
}
