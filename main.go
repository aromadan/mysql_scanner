package main

import (
	"flag"
	"fmt"
	"os"
)

func main() {
	host := flag.String("i", "localhost", "host IP")
	port := flag.String("p", "3306", "port")
	timeout := flag.Int("t", 10, "connection and read timeout")
	flag.Parse()

	fmt.Printf("connecting to %s:%s...\n", *host, *port)
	data, numBytes, err := GetPacket(*host, *port, *timeout)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Printf("connected.\nattemting to parse packet from server...\n")
	payload, err := TrimPacket(data, numBytes)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	mySQLInfo := &InitialHandshakePacket{}
	err = mySQLInfo.Decode(payload)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Printf("MySQL appears to be running on %s:%s\n\n", *host, *port)
	mySQLInfo.Print()
}
